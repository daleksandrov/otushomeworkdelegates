﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OtusHome.Delegates
{
    public static class ExtentionClass
    {
        /// <summary>
        /// Расширение для коллекции, возвращающее максимальный элемент
        /// </summary>
        /// <param name="list"></param>
        /// <param name="orderBy"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetMax<T>(this IEnumerable<T> list, Func<T, float> orderBy)
        {
            T maxitem = default(T);
            
            maxitem = list.OrderByDescending(orderBy).FirstOrDefault();
            
            // var max = float.MinValue;
            // foreach (var item in list)
            // {
            //     var x = orderBy(item);
            //     if (x > max)
            //     {
            //         max = x;
            //         maxitem = item;
            //     }
            // }

            return maxitem;
        }
    }
}